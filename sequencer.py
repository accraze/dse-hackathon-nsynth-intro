from pydub import AudioSegment
import glob
import random


def sequence(path, title):
    sounds = []
    # get all audio files in path
    audio_fnames = glob.glob(path+'/*.wav')
    print(audio_fnames)
    # shuffle files
    random.shuffle(audio_fnames)
    for fname in audio_fnames:
        sound = AudioSegment.from_wav(fname)
        sounds.append(sound)
    composition = None
    for s in sounds:
        if not composition:
            composition = s
        else:
            composition += s

    composition.export(title, format='wav')


if __name__ == '__main__':
    path = 'gen-audio/gen'
    title = '1951-test.wav'
    sequence(path, title)
