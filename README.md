# dse-hackathon-nsynth-intro

A simple python script to get started with generating raw audio using the [NSynth](https://github.com/magenta/magenta/tree/main/magenta/models/nsynth) Autoencoder model.

## Quickstart
First install Magenta:
```
pip install magenta
```

Next, download the Wavenet model checkpoints:
http://download.magenta.tensorflow.org/models/nsynth/wavenet-ckpt.tar

Move the model checkpoint file into the directory of your project and decompress the archive using tar:
```
tar -xvf wavenet-ckpt.tar
```

You should now have a new `wavenet-ckpt/` directory with model.ckpt-200000 metadata etc inside.

Here is the wav file I used in the video:
https://commons.wikimedia.org/wiki/File:Drum_(avk).wav
(feel free to use any other wav file -- smaller files seem to work best.)

## Generator

This script (`generator.py`) contains code that loads two audio files, interpolates the encodings and then passes the mashed u0 encodings to Nsynth to synthesize into raw audio.

## Sequencer
This script (`sequencer.py`) contains codes that searches a directory for .wav files and randomly sequences them together to form a new "song" or "composition". 
